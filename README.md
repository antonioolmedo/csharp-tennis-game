# Juego de Tenis

Este es un simple juego de tenis en el cual la máquina asignará a cada jugador un nivel de habilidad basado en un número aleatorio de 1 a 255 más bonus según atributos de cada jugador.

Según dicha habilidad, los jugadores podrán llegar a los golpes del jugador contrario y poder ejecutar un golpe, si falla en el alcance, el jugador perderá el punto.

Las reglas de puntuación están basadas en lo indicado en https://es.wikipedia.org/wiki/Tenis#Puntuaci.C3.B3n

