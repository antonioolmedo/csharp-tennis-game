﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TennisGame.Game;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisGameTests;

namespace TennisGame.Game.Tests
{
    [TestClass()]
    public class GameEngineTests
    {
        [TestMethod()]
        public void GetMaxSetsTest()
        {
            Creator.CreateGameTest();
            Assert.AreEqual(Creator.ge.GetMaxSets(), Creator.MaxSets);
        }

        [TestMethod()]
        public void PlayTest()
        {
            //  se prueba todo el partido, cuando termina damos por hecho que va todo bien, por lo menos sin excepciones
            Creator.CreateGameTest();
            Creator.ge.Play();
        }
    }
}