﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TennisGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisGameTests;

namespace TennisGame.Model.Tests
{
    [TestClass()]
    public class ScoreTests
    {
        [TestMethod()]
        public void WonMoveTest()
        {
            Creator.CreateGameTest();

            //  comprueba que suma bien los puntos
            Assert.AreEqual(0, Creator.p1.GetGameScore());
            Creator.score.WonMove(Creator.p1);
            Creator.score.WonMove(Creator.p1);
            Creator.score.WonMove(Creator.p2);
            Assert.AreEqual(30, Creator.p1.GetGameScore());
            Assert.AreEqual(15, Creator.p2.GetGameScore());

            //  comprueba que suma bien los juegos
            Creator.score.WonMove(Creator.p1);
            Creator.score.WonMove(Creator.p1);
            Assert.AreEqual(0, Creator.p1.GetGameScore());
            Assert.AreEqual(1, Creator.p1.GetGames());

            //  comprueba que suma bien los sets
            for (int i = 2; i<6; i++)
            {
                Creator.score.WonMove(Creator.p1);
                Creator.score.WonMove(Creator.p1);
                Creator.score.WonMove(Creator.p1);
                Creator.score.WonMove(Creator.p1);
                Assert.AreEqual(i, Creator.p1.GetGames());
            }

            //  el ultimo lo hacemos a mano, este juego gana el set
            Creator.score.WonMove(Creator.p1);
            Creator.score.WonMove(Creator.p1);
            Creator.score.WonMove(Creator.p1);
            Creator.score.WonMove(Creator.p1);
            Assert.AreEqual(0, Creator.p1.GetGames());
            Assert.AreEqual(1, Creator.p1.GetSets());
        }
    }
}