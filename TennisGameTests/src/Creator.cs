﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisGame.Game;
using TennisGame.Model;

namespace TennisGameTests
{
    public class Creator
    {
        public const int MaxSets = 3;
        public static Player p1, p2;
        public static GameEngine ge;
        public static Score score;
        public static Random rnd = new Random();

        public static void CreateGameTest()
        {
            p1 = new Player("Player1", 20, 180, rnd);
            p2 = new Player("Player2", 30, 250, rnd);
            ge = new GameEngine(p1, p2, 1, 3);
            ge.SetMsSleep(0);
            score = ge.GetScore();
        }
    }
}
