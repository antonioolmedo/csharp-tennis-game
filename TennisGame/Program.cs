﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisGame.Game;

namespace TennisGame
{
    class Program
    {
        private static bool Keep = true;

        static void Main(string[] args)
        {
            do
            {
                PrintMenu();
                int option = MakeSelection();
                Console.WriteLine("");
                switch (option)
                {
                    case 1:
                        GameEngine ge = GameEngineFactory.CreateGameEngine();
                        ge.Play();
                        break;
                    case 2:
                        PrintHelp();
                        break;
                    case 3:
                        Keep = false;
                        break;
                    default:
                        Console.WriteLine("Selección inválida.");
                        break;
                }
            } while (Keep);


        }

        private static void PrintMenu()
        {
            Console.WriteLine("1. Iniciar juego");
            Console.WriteLine("2. Ayuda");
            Console.WriteLine("3. Salir");
            Console.Write("Introduce la opción del menú: ");
        }

        private static int MakeSelection()
        {
            try
            {
                string input = Console.ReadLine();
                return Convert.ToUInt16(input);
            }
            catch (System.FormatException)
            {
                Console.WriteLine("Introduce un número.");
                return 0;
            }
        }

        private static void PrintHelp()
        {
            Console.WriteLine("");
            Console.WriteLine("**************** AYUDA ****************");
            Console.WriteLine("");
            Console.WriteLine("Autor: Antonio Olmedo Navajas");
            Console.WriteLine("Web: aolmedo.com");
            Console.WriteLine("Correo: dev@aolmedo.com");
            Console.WriteLine("Versión: 1.0.0");
            Console.WriteLine("Copyright: 2017 Antonio Olmedo Navajas");
            Console.WriteLine("Licencia: MIT");
            Console.WriteLine("");
            Console.WriteLine("Este es un simple juego de tenis en el cual la máquina asignará a cada jugador un nivel de habilidad");
            Console.WriteLine("basado en un número aleatorio de 1 a 255 más bonus según atributos de cada jugador.");
            Console.WriteLine("");
            Console.WriteLine("Según dicha habilidad, los jugadores podrán llegar a los golpes del jugador contrario y poder ejecutar un golpe,");
            Console.WriteLine("si falla en el alcance, el jugador perderá el punto.");
            Console.WriteLine("");
            Console.WriteLine("Las reglas de puntuación están basadas en lo indicado en https://es.wikipedia.org/wiki/Tenis#Puntuaci.C3.B3n");
            Console.WriteLine("");
            Console.WriteLine("***************************************");
            Console.WriteLine("");
        }
    }
}
