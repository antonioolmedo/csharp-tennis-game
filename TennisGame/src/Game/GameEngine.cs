﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisGame.Model;

namespace TennisGame.Game
{
    public class GameEngine
    {
        private bool playing = false;
        private bool needChangeKickoff = false;
        private Player playerStart, playerOther = null;
        private Score score = null;
        private int maxSets;
        private int msSleep = 2000;

        public GameEngine(Player p1, Player p2, int whoStarts, int maxSets)
        {
            this.maxSets = maxSets;
            this.GenerateKickoff(p1, p2, whoStarts);
            this.score = new Score(playerStart, playerOther, this);
        }

        public void SetMsSleep(int msSleep)
        {
            this.msSleep = msSleep;
        }

        private void GenerateKickoff(Player p1, Player p2, int whoStarts)
        {
            if (whoStarts == 1)
            {
                this.playerStart = p1;
                this.playerOther = p2;
            }
            else
            {
                this.playerStart = p2;
                this.playerOther = p1;
            }
        }

        public void StopPlay()
        {
            this.playing = false;
        }

        public void NeedChangeKickoff()
        {
            this.needChangeKickoff = true;
        }

        public int GetMaxSets()
        {
            return this.maxSets;
        }

        public void Play()
        {
            this.playing = true;
            Console.WriteLine("Empieza el jugador " + playerStart.GetName());

            do
            {
                System.Threading.Thread.Sleep(msSleep);
                this.score.WonMove(GameMove(playerStart, playerOther));
                if (this.needChangeKickoff && this.playing)//  se acaba el juego cambiamos el player que saca
                {
                    this.ChangeKickoff();
                }
            } while (this.playing);

        }

        private void ChangeKickoff()
        {
            Player pivotPlayer = playerStart;
            playerStart = playerOther;
            playerOther = pivotPlayer;
            Console.WriteLine("Saca el jugador: " + playerStart.GetName());
            this.needChangeKickoff = false;
            pivotPlayer = null;
        }

        private static Player GameMove(Player playerStart, Player otherPlayer)
        {
            int psHit = playerStart.Hit();
            int poGiveBack = otherPlayer.Hit();

            Console.WriteLine("El jugador " + playerStart.GetName() + " golpea con: " + psHit);
            Console.WriteLine("El jugador " + otherPlayer.GetName() + " lo intenta con: " + poGiveBack);
            if (poGiveBack > psHit)
            {
                return GameMove(otherPlayer, playerStart);
            }

            return playerStart;
        }

        public Score GetScore()
        {
            return this.score;
        }
    }
}
