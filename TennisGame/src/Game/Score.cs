﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisGame.Game;

namespace TennisGame.Model
{
    public class Score
    {
        private Player p1, p2;
        private GameEngine gameEngine;
        private string[] scores;
        private string actualGame;
        private int actualSet = 0;

        public Score(Player p1, Player p2, GameEngine gameEngine)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.gameEngine = gameEngine;
            this.InitScores();
        }

        private void InitScores()
        {
            this.scores = new string[this.gameEngine.GetMaxSets()];
            for (int i = 0; i < this.scores.Length; i++)
            {
                this.scores[i] = "(" + this.p1.GetName() + " 0 - 0 " + this.p2.GetName() + ")"; ;
            }
        }

        public void WonMove(Player p)
        {
            Console.WriteLine("El jugador " + p.GetName() + " gana.");
            if (p == this.p1)
            {
                this.CheckScores(this.p1, this.p2);
            }
            else
            {
                this.CheckScores(this.p2, this.p1);
            }
        }

        private void ResetGameScores()
        {
            this.p1.SetGameScore(0);
            this.p1.RemoveGameOnAdv();
            this.p2.SetGameScore(0);
            this.p2.RemoveGameOnAdv();
        }

        private void ResetGames()
        {
            if (this.actualSet <= this.gameEngine.GetMaxSets() - 1)
            {
                this.actualSet++;
            }
            this.p1.ResetGames();
            this.p2.ResetGames();
        }

        private void CheckScores(Player playerWonLastMove, Player otherPlayer)
        {
            //  si el otro jugador está en ventaja, se le quita porque el que gana el movimiento la quita
            if (otherPlayer.IsGameOnAdv() && playerWonLastMove.GetGameScore() == 40)
            {
                otherPlayer.RemoveGameOnAdv();
                this.GetGameScores();
            }
            //  si ambos players tienen 40 puntos, el que gana el movimiento tiene ventaja
            else if (playerWonLastMove.GetGameScore() == 40 && !playerWonLastMove.IsGameOnAdv() && otherPlayer.GetGameScore() == 40)
            {
                playerWonLastMove.GameOnAdv();
                this.GetGameScores();
            }
            //  si el que gana el movimiento está en ventaja, se resetea los scores actuales y se le incrementa un juego
            else if (playerWonLastMove.IsGameOnAdv() || playerWonLastMove.GetGameScore() == 40)
            {
                playerWonLastMove.IncrementGames();
                Console.WriteLine("El jugador " + playerWonLastMove.GetName() + " ha ganado el juego.");
                this.GetScores();
                this.gameEngine.NeedChangeKickoff();
                ResetGameScores();
                this.CheckSets();
            }
            else
            {
                playerWonLastMove.SetGameScore(GetNextGameScore(playerWonLastMove.GetGameScore()));
                this.GetGameScores();
            }
        }

        private void CheckSets()
        {
            int difference = this.p1.GetGames() - this.p2.GetGames();

            if (difference >= 2 && this.p1.GetGames() >= 6)
            {
                this.p1.IncrementSets();
                this.ResetGames();
            }
            else if (difference <= -2 && this.p2.GetGames() >= 6)
            {
                this.p2.IncrementSets();
                this.ResetGames();
            }

            if (this.actualSet >= this.gameEngine.GetMaxSets()) 
            {
                this.gameEngine.StopPlay();
                this.WhoWon();
            }
        }

        private static int GetNextGameScore(int actualScore)
        {
            switch (actualScore)
            {
                case 0:
                    return 15;
                case 15:
                    return 30;
                case 30:
                case 40:
                    return 40;
                default:
                    return 0;
            }
        }

        private void GetScores()
        {

            Console.WriteLine("");
            Console.WriteLine("-----------------Resumen-----------------");
            this.scores[this.actualSet] = "(" + this.p1.GetName() + " " + this.p1.GetGames() + " - " + this.p2.GetGames() + " " + this.p2.GetName() + ")";
            for(int i=0; i<this.scores.Length; i++)
            {
                Console.WriteLine(this.scores[i]);
            }
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("");
        }

        private void GetGameScores()
        {
            Console.WriteLine("");
            Console.WriteLine("-------------Resumen del juego-----------");
            this.actualGame = this.p1.GetName() + " " + this.GetPlayerGameScore(this.p1) + " - " + this.GetPlayerGameScore(this.p2) + " " + this.p2.GetName();
            Console.WriteLine(this.actualGame + " " + this.scores[this.actualSet]);
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("");
        }

        private string GetPlayerGameScore(Player p)
        {
            if (p.IsGameOnAdv())
            {
                return "Adv";
            }

            return Convert.ToString(p.GetGameScore());
        }

        private void WhoWon()
        {
            Player whoWon = this.p2;
            if (this.p1.GetSets() >= this.p2.GetSets())
            {
                whoWon = this.p1;
            }

            Console.WriteLine("");
            Console.WriteLine("El jugador " + whoWon.GetName() + " ganó el partido.");
            Console.WriteLine("");
        }
    }
}
