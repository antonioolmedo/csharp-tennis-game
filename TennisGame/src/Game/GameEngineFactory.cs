﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TennisGame.Model;

namespace TennisGame.Game
{
    class GameEngineFactory
    {
        public static GameEngine CreateGameEngine()
        {
            Random rnd = new Random();
            Player p1, p2;
            int maxSets;
            maxSets = GetMaxSets();

            p1 = CreatePlayer(1, rnd);
            p2 = CreatePlayer(2, rnd);

            Console.WriteLine("-----------------Datos-------------------");
            Console.WriteLine(p1.GetName() + " tiene " + p1.GetSkill() + " puntos de habilidad.");
            Console.WriteLine(p2.GetName() + " tiene " + p2.GetSkill() + " puntos de habilidad.");
            Console.WriteLine("-----------------------------------------");
            Console.WriteLine("");

            return new GameEngine(p1, p2, rnd.Next(1, 3), maxSets);
        }

        private static Player CreatePlayer(int playerNumber, Random rnd)
        {
            string playerName;
            int age, height;
            bool playerSetted = false;
            Player p = null;

            do
            {
                try
                {
                    Console.Write("Introduce el nombre del jugador " + playerNumber + ": ");
                    playerName = Console.ReadLine();
                    Console.Write("Introduce la edad del jugador " + playerNumber + ": ");
                    age = Convert.ToUInt16(Console.ReadLine());
                    Console.Write("Introduce la estatura del jugador " + playerNumber + " (cm): ");
                    height = Convert.ToUInt16(Console.ReadLine());
                    p = new Player(playerName, age, height, rnd);
                    playerSetted = true;
                }
                catch (Exception)
                {
                    Console.WriteLine("Valor inválido, empiece de nuevo.");
                }
            } while (!playerSetted);
            
            return p;

        }

        private static int GetMaxSets()
        {
            int maxSets = 0;
            bool maxSetsSetted = false;

            do
            {
                try
                {
                    Console.Write("Introduce el número de sets (3 ó 5): ");
                    maxSets = Convert.ToInt16(Console.ReadLine());

                    if (maxSets == 3 || maxSets == 5)
                    {
                        maxSetsSetted = true;
                    }
                    else
                    {
                        Console.WriteLine("Valor inválido, debe ser 3 o 5.");
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("Valor inválido, pruebe de nuevo.");
                }
            } while (!maxSetsSetted);

            return maxSets;
        }
    }
}
