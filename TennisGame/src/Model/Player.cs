﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TennisGame.Model
{
    public class Player
    {
        private const int MaxSkill = 255;

        private string name;
        private int skill;
        private int games;
        private int sets;
        private int gameScore;
        private bool gameOnAdv;
        private Random rnd;
        private int age;
        private int height;

        public Player(string name, int age, int height, Random rnd)
        {
            this.name = name;
            this.age = age;
            this.height = height;
            this.games = 0;
            this.gameScore = 0;
            this.gameOnAdv = false;
            this.rnd = rnd;
            this.GenerateSkill();
        }

        public int Hit()
        {
            return this.rnd.Next(1, this.GetSkill());
        }

        public void IncrementGames()
        {
            this.games++;
        }

        public void IncrementSets()
        {
            this.sets++;
        }

        public void ResetGames()
        {
            this.games = 0;
        }

        private void GenerateSkill()
        {
            int randomValue = rnd.Next(1, MaxSkill);

            if (this.height >= 180)
            {
                randomValue += 10;
            }

            if (this.age <= 50)
            {
                randomValue += 10;
            }

            this.skill = randomValue;
        }

        public void GameOnAdv()
        {
            this.gameOnAdv = true;
        }

        public void RemoveGameOnAdv()
        {
            this.gameOnAdv = false;
        }

        public int GetSkill()
        {
            return this.skill;
        }

        public string GetName()
        {
            return this.name;
        }

        public int GetGames()
        {
            return this.games;
        }

        public void SetGameScore(int gameScore)
        {
            this.gameScore = gameScore;
        }

        public int GetGameScore()
        {
            return this.gameScore;
        }

        public bool IsGameOnAdv()
        {
            return this.gameOnAdv;
        }

        public int GetSets()
        {
            return this.sets;
        }
    }
}
